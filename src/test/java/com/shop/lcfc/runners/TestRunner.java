package com.shop.lcfc.runners;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(

        features = {"src/test/java/com/shop/lcfc/features"},
        glue = {"com/shop/lcfc/hooks", "com/shop/lcfc/stepDefinitions"},
        publish = true
)

public class TestRunner {
}
