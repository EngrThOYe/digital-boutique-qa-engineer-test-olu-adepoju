Feature: Product checkout - Click and collect delivery

  Scenario: Checking out product with click and collect delivery option
    Given I navigate to the Checkout welcome page
    When I click on CONTINUE TO DELIVERY button
    And the shipping page is displayed
    And I click on click and collect delivery option
    And I click next button
    Then the Payment page is displayed
    And I can click on CONTINUE button to make payment
