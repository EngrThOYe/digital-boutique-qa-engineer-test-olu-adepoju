Feature: Path to sign into the user's account

  Scenario Outline: Signing into user's account with valid email and password
    Given I navigate to shop.lcfc.com page
    When I click on Sign In or Register link
    And Sign In page is displayed
    And I enter the valid Sign In Details Email as "<Email>"
    And I enter the valid Sign In Details Password as "<Password>"
    And I click on the SIGN IN button
    Then the user is signed In

    Examples:
      | Email          | Password     |
      | abcxyz@abc.xyz | zaxbycAX@926 |