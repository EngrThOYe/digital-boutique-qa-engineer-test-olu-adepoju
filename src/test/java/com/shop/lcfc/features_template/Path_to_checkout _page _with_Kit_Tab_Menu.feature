Feature: Path to checkout page with Kit Tab Menu

  Scenario Outline: checking out product without personalizing (size only)
    Given I navigate to the signed in shop.lcfc.com page
    When I click on Kit link tab
    And the kit type page is displayed
    And I click on any of the kit type as "<Kit_Type>"
    And the kit type page is displayed
    And I click on any of the kit products
    And the kit product page is displayed
    And Choose an option for size as "<Size>"
    And I click on QTY texbox
    And I enter a valid Quantity as "<QTY>"
    And I click on ADD TO CART button
    And I click Checkout button
    Then the Checkout welcome page is displayed
    And I can click on CONTINUE TO DELIVERY button

    Examples:
      | Kit_Type              | Size | QTY |
      | SHOP 2021/22 HOME KIT | XL   | 1   |
      | SHOP 2021/22 AWAY KIT | L    | 2   |
