Feature: Path to register a new account

  Scenario Outline: Registering a new user with valid email and password
    Given I navigate to shop.lcfc.com page
    When I click on Sign In or Register link
    And Sign In page is displayed
    And I click on Register-link
    And Register page is displayed
    And I enter the valid First Name as "<First_Name>"
    And I enter the valid Surname as "<Surname>"
    And I enter the valid Email as "<Email>"
    And I enter the valid Confirm Email as "<Comfirm_Email>"
    And I enter the valid Password as "<Password>"
    And I enter the valid Confirm Password as "<Comfirm_Password>"
    And I select the checkbox for Opt-In to LCFC as "<Opt_In_to_LCFC>"
    And I select the checkbox for Opt-In to Partners as "<Opt_In_to_Partners>"
    And I click on the Register button
    Then the AwaitingConfirmEmail page is displayed

    Examples:
      | First_Name | Surname | Email          | Comfirm_Email  | Password     | Comfirm_Password | Opt_In_to_LCFC | Opt_In_to_Partners |
      | ABC        | XYZ     | abcxyz@abc.xyz | abcxyz@abc.xyz | zaxbycAX@926 | zaxbycAX@926     | Yes            | No                 |

