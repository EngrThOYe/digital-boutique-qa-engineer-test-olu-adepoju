Feature: Personalising kit

  Scenario Outline: Adding your name with sleeve Badge
    Given I navigate to the kit type page
    When Choose an option for size as "<Size>"
    And I select Personalise as ADD YOUR NAME
    And I click on ADD YOUR NAME texbox
    And I enter a valid name as "<YOUR_NAME>"
    And I click on ADD YOUR NUMBER texbox
    And I enter a valid number as "<YOUR_NUMBER>"
    And I select badge as "<badge>"
    And I select Free Sponsor badge included
    And I click on QTY texbox
    And I enter a valid Quantity as "<QTY>"
    And I click on ADD TO CART button
    And I click Checkout button
    Then the Checkout welcome page is displayed
    And I can click on CONTINUE TO DELIVERY button

    Examples:
      | Size | YOUR_NAME | YOUR_NUMBER | badge           | QTY |
      | XL   | ABC XYZ   | 03          | Premier League  | 2   |
      | L    | XY ABD    | 53          | Emirates FA Cup | 1   |

  Scenario Outline: Adding your name without sleeve Badge
    Given I navigate to the kit type page
    When Choose an option for size as "<Size>"
    And I select Personalise as ADD YOUR NAME
    And I click on ADD YOUR NAME texbox
    And I enter a valid name as "<YOUR_NAME>"
    And I click on ADD YOUR NUMBER texbox
    And I enter a valid number as "<YOUR_NUMBER>"
    And I select badge No badge
    And I select Free Sponsor badge included
    And I select Font as "<Font>"
    And I click on QTY texbox
    And I enter a valid Quantity as "<QTY>"
    And I click on ADD TO CART button
    And I click Checkout button
    Then the Checkout welcome page is displayed
    And I can click on CONTINUE TO DELIVERY button

    Examples:
      | Size | YOUR_NAME | YOUR_NUMBER | Font           | QTY |
      | XL   | ABC XYZ   | 03          | Premier League | 2   |
      | L    | XY ABD    | 53          | Cup Font       | 1   |

  Scenario Outline: Adding player name without sleeve Badge
    Given I navigate to the kit type page
    When Choose an option for size as "<Size>"
    And I select Personalise as ADD PLAYERS NAME
    And I select a Team type as "<Team_Type>"
    And I select any of the player name
    And I select badge No badge
    And I select Free Sponsor badge included
    And I select Font as "<Font>"
    And I click on QTY texbox
    And I enter a valid Quantity as "<QTY>"
    And I click on ADD TO CART button
    And I click Checkout button
    Then the Checkout welcome page is displayed
    And I can click on CONTINUE TO DELIVERY button

    Examples:
      | Size | Team_Type         | Font           | QTY |
      | XL   | First Team        | Premier League | 2   |
      | L    | LCFC Women's Team | Cup Font       | 1   |

  Scenario Outline: Adding player name with sleeve Badge
    Given I navigate to the kit type page
    When Choose an option for size as "<Size>"
    And I select Personalise as ADD PLAYERS NAME
    And I select a Team type as "<Team_Type>"
    And I select any of the player name
    And I select badge as "<badge>"
    And I select Free Sponsor badge included
    And I click on QTY texbox
    And I enter a valid Quantity as "<QTY>"
    And I click on ADD TO CART button
    And I click Checkout button
    Then the Checkout welcome page is displayed
    And I can click on CONTINUE TO DELIVERY button

    Examples:
      | Size | Team_Type         | badge           | QTY |
      | XL   | First Team        | Premier League  | 2   |
      | L    | LCFC Women's Team | Emirates FA Cup | 1   |