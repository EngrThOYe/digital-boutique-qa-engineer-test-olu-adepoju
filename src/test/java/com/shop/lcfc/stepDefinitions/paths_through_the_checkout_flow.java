package com.shop.lcfc.stepDefinitions;

import com.shop.lcfc.pages.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class paths_through_the_checkout_flow extends BasePage {

    HomePage homePage = new HomePage(driver);
    SigninPage signinPage = new SigninPage(driver);
    KitTypePage kitTypePage = new KitTypePage(driver);
    Productpage productpage = new Productpage(driver);
    PersonalisationPage personalisationPage = new PersonalisationPage(driver);
    CheckoutPage checkoutPage =new CheckoutPage(driver);


    @Given("I navigate to shop.lcfc.com page")
    public void i_navigate_to_shop_lcfc_com_page() throws InterruptedException {
        NavigateToHomePage();
        Thread.sleep(2000);

    }

    @When("I click on Sign In or Register link")
    public void i_click_on_sign_in_or_register_link() {
        signinPage = homePage.clickonSignInorRegister();

    }

    @When("Sign In page is displayed")
    public void sign_in_page_is_displayed() {

    }

    @When("I click on Register-link")
    public void i_click_on_register_link() {

    }

    @When("Register page is displayed")
    public void register_page_is_displayed() {

    }

    @When("I enter the valid First Name as {string}")
    public void i_enter_the_valid_first_name_as(String string) {

    }

    @When("I enter the valid Surname as {string}")
    public void i_enter_the_valid_surname_as(String string) {

    }

    @When("I enter the valid Email as {string}")
    public void i_enter_the_valid_email_as(String string) {

    }

    @When("I enter the valid Confirm Email as {string}")
    public void i_enter_the_valid_confirm_email_as(String string) {

    }

    @When("I enter the valid Password as {string}")
    public void i_enter_the_valid_password_as(String string) {

    }

    @When("I enter the valid Confirm Password as {string}")
    public void i_enter_the_valid_confirm_password_as(String string) {

    }

    @When("I select the checkbox for Opt-In to LCFC as {string}")
    public void i_select_the_checkbox_for_opt_in_to_lcfc_as(String string) {

    }

    @When("I select the checkbox for Opt-In to Partners as {string}")
    public void i_select_the_checkbox_for_opt_in_to_partners_as(String string) {

    }

    @When("I click on the Register button")
    public void i_click_on_the_register_button() {

    }

    @Then("the AwaitingConfirmEmail page is displayed")
    public void the_awaiting_confirm_email_page_is_displayed() {

    }

    @When("I enter the valid Sign In Details Email as {string}")
    public void i_enter_the_valid_sign_in_details_email_as(String Email) throws InterruptedException {
        signinPage.clickEmailtexbox();
        Thread.sleep(1000);
        signinPage.Enter_email(Email);

    }

    @When("I enter the valid Sign In Details Password as {string}")
    public void i_enter_the_valid_sign_in_details_password_as(String Password) throws InterruptedException {
        signinPage.clickpasswordtexbox();
        Thread.sleep(1000);
        signinPage.Enter_password(Password);

    }

    @When("I click on the SIGN IN button")
    public void i_click_on_the_sign_in_button() {
        homePage = signinPage.clickSignInButton();

    }

    @Then("the user is signed In")
    public void the_user_is_signed_in() {

    }

    @Given("I navigate to the signed in shop.lcfc.com page")
    public void i_navigate_to_the_signed_in_shop_lcfc_com_page() {

    }

    @When("I click on Kit link tab")
    public void i_click_on_kit_link_tab() {
        kitTypePage = homePage.ClickKitLinkTab();

    }

    @When("the kit type page is displayed")
    public void the_kit_type_page_is_displayed() {

    }

    @When("I click on any of the kit type as {string}")
    public void i_click_on_any_of_the_kit_type_as(String Kit_Type) {
        productpage = kitTypePage.selectKitType(Kit_Type);

    }

    @When("I click on any of the kit products")
    public void i_click_on_any_of_the_kit_products() {
        personalisationPage = productpage.clickResult();

    }

    @When("the kit product page is displayed")
    public void the_kit_product_page_is_displayed() {
        productpage.isSearchResultPageDisplayed();

    }

    @When("Choose an option for size as {string}")
    public void choose_an_option_for_size_as(String string) {
        personalisationPage.ChooseSize();

    }

    @When("I click on QTY texbox")
    public void i_click_on_qty_texbox() {

    }

    @When("I enter a valid Quantity as {string}")
    public void i_enter_a_valid_quantity_as(String QTY) {
        personalisationPage.enterValidQuantity(QTY);

    }

    @When("I click on ADD TO CART button")
    public void i_click_on_add_to_cart_button() throws InterruptedException {
        personalisationPage.clickADDtoCARTbutton();
        Thread.sleep(2000);

    }

    @When("I click Checkout button")
    public void i_click_checkout_button() throws InterruptedException {
        checkoutPage = personalisationPage.clickCheckoutButton();
        Thread.sleep(3000);
    }

    @Then("the Checkout welcome page is displayed")
    public void the_checkout_welcome_page_is_displayed() {

    }

    @Then("I can click on CONTINUE TO DELIVERY button")
    public void i_can_click_on_continue_to_delivery_button() throws InterruptedException {
        checkoutPage.clickCONTINUEtoDELIVERY();
        Thread.sleep(2000);

    }

    @Given("I navigate to the kit type page")
    public void i_navigate_to_the_kit_type_page() {

    }

    @When("I select Personalise as ADD YOUR NAME")
    public void i_select_personalise_as_add_your_name() {

    }

    @When("I click on ADD YOUR NAME texbox")
    public void i_click_on_add_your_name_texbox() {

    }

    @When("I enter a valid name as {string}")
    public void i_enter_a_valid_name_as(String string) {

    }

    @When("I click on ADD YOUR NUMBER texbox")
    public void i_click_on_add_your_number_texbox() {

    }

    @When("I enter a valid number as {string}")
    public void i_enter_a_valid_number_as(String string) {

    }

    @When("I select badge as {string}")
    public void i_select_badge_as(String string) {

    }

    @When("I select Free Sponsor badge included")
    public void i_select_free_sponsor_badge_included() {

    }

    @When("I select badge No badge")
    public void i_select_badge_no_badge() {

    }

    @When("I select Font as {string}")
    public void i_select_font_as(String string) {

    }

    @When("I select Personalise as ADD PLAYERS NAME")
    public void i_select_personalise_as_add_players_name() {

    }

    @When("I select a Team type as {string}")
    public void i_select_a_team_type_as(String string) {

    }

    @When("I select any of the player name")
    public void i_select_any_of_the_player_name() {

    }

    @Given("I navigate to the Checkout welcome page")
    public void i_navigate_to_the_checkout_welcome_page() {

    }

    @When("I click on CONTINUE TO DELIVERY button")
    public void i_click_on_continue_to_delivery_button() {

    }

    @When("the shipping page is displayed")
    public void the_shipping_page_is_displayed() {

    }

    @When("I click on home delivery option")
    public void i_click_on_home_delivery_option() {
        checkoutPage.clickonHomeDelivery();

    }

    @When("I enter a valid receiver First Name as {string}")
    public void i_enter_a_valid_receiver_first_name_as(String R_First_Name) throws InterruptedException {

    }

    @When("I enter a valid receiver Last Name as {string}")
    public void i_enter_a_valid_receiver_last_name_as(String string) {

    }

    @When("I enter a valid Company as {string}")
    public void i_enter_a_valid_company_as(String string) {

    }

    @When("I enter a valid Street Address Line {int} as {string}")
    public void i_enter_a_valid_street_address_line_as(Integer int1, String string) {

    }

    @When("I select Country as {string}")
    public void i_select_country_as(String string) {

    }

    @When("I enter a valid State or Province as {string}")
    public void i_enter_a_valid_state_or_province_as(String string) {

    }

    @When("I enter a valid City as {string}")
    public void i_enter_a_valid_city_as(String string) {

    }

    @When("I enter a valid Zip or Postal Code as {string}")
    public void i_enter_a_valid_zip_or_postal_code_as(String string) {

    }

    @When("I enter a valid Phone Number as {string}")
    public void i_enter_a_valid_phone_number_as(String string) {
;
    }

    @When("I select Add Delivery Methods {string}")
    public void i_select_add_delivery_methods(String Delivery_Methods) {
        checkoutPage.selectDeliveryMethods(Delivery_Methods);

    }

    @When("I click next button")
    public void i_click_next_button() throws InterruptedException {
        checkoutPage.clickNextButton();
        Thread.sleep(4000);

    }

    @Then("the Payment page is displayed")
    public void the_payment_page_is_displayed() {

    }

    @Then("I can click on CONTINUE button to make payment")
    public void i_can_click_on_continue_button_to_make_payment() {
        checkoutPage.clickonCONTINUEbutton();

    }

    @When("I click on click and collect delivery option")
    public void i_click_on_click_and_collect_delivery_option() {

    }


    @When("I click CONTINUE button to accept Cookies")
    public void iClickCONTINUEButtonToAcceptCookies() {
        homePage.acceptCookies();
    }
}
