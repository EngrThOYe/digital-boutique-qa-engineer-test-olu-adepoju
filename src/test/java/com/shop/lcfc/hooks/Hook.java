package com.shop.lcfc.hooks;

import com.shop.lcfc.commons.BrowserManager;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hook extends BrowserManager
{
    @Before
    public void setup()
    {
        launchBrowser("Chrome");
    }

    //@After
    public void tearDown()
    {
        closeBrowser();
    }
}
