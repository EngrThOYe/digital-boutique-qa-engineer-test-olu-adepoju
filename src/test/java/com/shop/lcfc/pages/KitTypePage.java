package com.shop.lcfc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Objects;

public class KitTypePage extends BasePage {
    public KitTypePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[1]/div/div/a")
    private WebElement ClickSHOP2021_22HOME_KIT;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[2]/div/div/a")
    private WebElement ClickSHOP2021_22AWAY_KIT;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[3]/div/div/a")
    private WebElement ClickSHOP2021_22THIRD_KIT;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[4]/div/div/a")
    private WebElement ClickSHOP2021_22GOALKEEPER_KIT;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[5]/div/div/a")
    private WebElement ClickSHOP2021_22ALL_KIT;

    private void ClickSHOP2021_22HOME_KIT() {
        ClickSHOP2021_22HOME_KIT.click();
    }
    private void ClickSHOP2021_22AWAY_KIT() {
        ClickSHOP2021_22AWAY_KIT.click();
    }
    private void ClickSHOP2021_22THIRD_KIT() {
        ClickSHOP2021_22THIRD_KIT.click();
    }
    private void ClickSHOP2021_22GOALKEEPER_KIT() {
        ClickSHOP2021_22GOALKEEPER_KIT.click();
    }
    private void ClickSHOP2021_22ALL_KIT() {
        ClickSHOP2021_22ALL_KIT.click();
    }

    public Productpage selectKitType(String Kit_Type) {
        if (Objects.equals(Kit_Type, "SHOP 2021/22 HOME KIT")) {
            ClickSHOP2021_22HOME_KIT();
        } else if (Objects.equals(Kit_Type, "SHOP 2021/22 AWAY KIT")) {
            ClickSHOP2021_22AWAY_KIT();
        } else if (Objects.equals(Kit_Type, "SHOP 2021/22 THIRD KIT")) {
            ClickSHOP2021_22THIRD_KIT();
        } else if (Objects.equals(Kit_Type, "SHOP 2021/22 GOALKEEPER KIT")) {
            ClickSHOP2021_22GOALKEEPER_KIT();
        } else if (Objects.equals(Kit_Type, "SHOP 2021/22 ALL KIT")) {
            ClickSHOP2021_22ALL_KIT();
        }
        return new Productpage(driver);
    }


}
