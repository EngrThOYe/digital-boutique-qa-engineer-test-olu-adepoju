package com.shop.lcfc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Objects;

public class CheckoutPage extends BasePage {
    public CheckoutPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//*[@id=\"welcome-step\"]/form/div/div/button/span/span")
    private WebElement clickCONTINUEtoDELIVERY;

    @FindBy(xpath = "//*[@id=\"shipping-option\"]/div[2]/div[1]")
    private WebElement clickonHomeDelivery;

    @FindBy(xpath = "//")
    private WebElement ENTERreceiverFirstName;

    @FindBy(xpath = "//")
    private WebElement ENTERreceiverLastName;

    @FindBy(xpath = "//")
    private WebElement enterCompanyName;

    @FindBy(xpath = "//")
    private WebElement ENTERStreetAddressLine1;

    @FindBy(xpath = "//")
    private WebElement ENTERStreetAddressLine2;

    @FindBy(xpath = "//")
    private WebElement ENTERStreetAddressLine3;

    @FindBy(xpath = "//")
    private WebElement selectCountry;

    @FindBy(xpath = "//")
    private WebElement enterStateProvince;

    @FindBy(xpath = "//")
    private WebElement enterCity;

    @FindBy(xpath = "//")
    private WebElement enterZipPostalCode;

    @FindBy(xpath = "//")
    private WebElement enterPhoneNumber;

    @FindBy(xpath = "//*[@id=\"checkout-shipping-method-load\"]/table/tbody/tr[1]/td[1]/label")
    private WebElement selectROYAL_MAIL;

    @FindBy(xpath = "//*[@id=\"checkout-shipping-method-load\"]/table/tbody/tr[2]/td[1]/label")
    private WebElement selectDHL_EXPRESS;

    @FindBy(xpath = "//*[@id=\"shipping-method-buttons-container\"]/div/button")
    private WebElement clickNextButton;

    @FindBy(xpath = "//*[@id=\"sagepaysuiteserver-actions-toolbar\"]")
    private WebElement clickonCONTINUEbutton;


    public void clickCONTINUEtoDELIVERY() {
        clickCONTINUEtoDELIVERY.click();
    }
    public void clickonHomeDelivery() {
        clickonHomeDelivery.click();
    }
    public void ENTERreceiverFirstName(String R_First_Name) {
        ENTERreceiverFirstName.click();
        ENTERreceiverFirstName.clear();
        ENTERreceiverFirstName.sendKeys(R_First_Name);
    }
    public void ENTERreceiverLastName(String R_Last_Name) {
        ENTERreceiverLastName.clear();
        ENTERreceiverLastName.sendKeys(R_Last_Name);
    }
    public void enterCompanyName(String Company) {
        enterCompanyName.clear();
        enterCompanyName.sendKeys(Company);
    }
    public void ENTERStreetAddressLine1(String Street_Address_Line_1) {
        ENTERStreetAddressLine1.clear();
        ENTERStreetAddressLine1.sendKeys(Street_Address_Line_1);
    }
    public void ENTERStreetAddressLine2(String Street_Address_Line_2) {
        ENTERStreetAddressLine2.clear();
        ENTERStreetAddressLine2.sendKeys(Street_Address_Line_2);
    }
    public void ENTERStreetAddressLine3(String Street_Address_Line_3) {
        ENTERStreetAddressLine3.clear();
        ENTERStreetAddressLine3.sendKeys(Street_Address_Line_3);
    }
    public void selectCountry(String Country)
    {
        selectByText(selectCountry, Country);
    }
    public void enterStateProvince(String State_Province) {
        enterStateProvince.clear();
        enterStateProvince.sendKeys(State_Province);
    }
    public void enterCity(String City) {
        enterCity.clear();
        enterCity.sendKeys(City);
    }
    public void enterZipPostalCode(String Zip_Postal_Code) {
        enterZipPostalCode.clear();
        enterZipPostalCode.sendKeys(Zip_Postal_Code);
    }
    public void enterPhoneNumber(String Phone_Number) {
        enterPhoneNumber.clear();
        enterPhoneNumber.sendKeys(Phone_Number);
    }
    private void selectROYAL_MAIL()
    {
        selectROYAL_MAIL.click();
    }
    private void selectDHL_EXPRESS()
    {
        selectDHL_EXPRESS.click();
    }
    public void selectDeliveryMethods(String Delivery_Methods)
    {
        if(Objects.equals(Delivery_Methods, "ROYAL MAIL TRACKED"))
        {
            selectROYAL_MAIL();
        }
        else if(Objects.equals(Delivery_Methods, "DHL EXPRESS"))
        {
            selectDHL_EXPRESS();
        }
    }
    public void clickNextButton() {
        clickNextButton.click();
    }
    public void clickonCONTINUEbutton() {
        clickonCONTINUEbutton.click();
    }
}
