package com.shop.lcfc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SigninPage extends BasePage {

    public SigninPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id=\"Email\"]")
    private WebElement clickEmailtexbox;

    @FindBy(xpath = "//*[@id=\"Email\"]")
    private WebElement Enter_email;

    @FindBy(xpath = "//*[@id=\"Password\"]")
    private WebElement clickpasswordtexbox;

    @FindBy(xpath = "//*[@id=\"Password\"]")
    private WebElement Enter_password;

    @FindBy(xpath = "//*[@id=\"submitForm\"]")
    private WebElement clickSignInButton;

    public void clickEmailtexbox() {
        clickEmailtexbox.click();
    }
    public void Enter_email(String Email) {
        Enter_email.sendKeys(Email);
    }
    public void clickpasswordtexbox() {
        clickpasswordtexbox.click();
    }
    public void Enter_password(String Password) {
        Enter_password.sendKeys(Password);
    }
    public HomePage clickSignInButton() {
        clickSignInButton.click();
        return new HomePage(driver);

    }
}
