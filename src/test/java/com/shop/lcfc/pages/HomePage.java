package com.shop.lcfc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage

{
    public HomePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    @FindBy(xpath = "//*[@id=\"btn-cookie-allow\"]/span")
    private WebElement acceptCookies;

    @FindBy(xpath = "//*[@id=\"mm-0\"]/div[2]/div[1]/header/div[1]/ul/li[2]/a/span")
    private WebElement clickonSignInorRegister;

    @FindBy(xpath = "//*[@id=\"js-nav\"]/ul/li[1]/a/span")
    private WebElement ClickKitLinkTab;

    public void acceptCookies() {
        acceptCookies.click();
    }
    public SigninPage clickonSignInorRegister() {
        clickonSignInorRegister.click();
        return new SigninPage(driver);
    }
    public KitTypePage ClickKitLinkTab() {
        ClickKitLinkTab.click();
        return new KitTypePage(driver);
    }
}
