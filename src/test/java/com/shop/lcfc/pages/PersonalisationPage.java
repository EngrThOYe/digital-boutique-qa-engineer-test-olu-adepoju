package com.shop.lcfc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonalisationPage extends BasePage {
    public PersonalisationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//*[@id=\"attribute127\"]/option[2]")
    private WebElement ChooseSize;

    @FindBy(xpath = "//*[@id=\"qty\"]")
    private WebElement enterValidQuantity;

    @FindBy(xpath = "//*[@id=\"product-addtocart-button\"]")
    private WebElement clickADDtoCARTbutton;

    @FindBy(xpath = "//*[@id=\"top-cart-btn-checkout\"]")
    private WebElement clickCheckoutButton;

    public void ChooseSize() {
        ChooseSize.click();
    }
    public void enterValidQuantity(String QTY) {
        enterValidQuantity.clear();
        enterValidQuantity.sendKeys(QTY);
    }
    public void clickADDtoCARTbutton() {
        clickADDtoCARTbutton.click();
    }
    public CheckoutPage clickCheckoutButton() {
        clickCheckoutButton.click();
        return new CheckoutPage(driver);
    }
}