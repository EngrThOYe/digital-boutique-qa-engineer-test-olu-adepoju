package com.shop.lcfc.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Productpage extends BasePage{
    public Productpage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "product-image-photo-hover")
    private List<WebElement> Productresults;

    public void isSearchResultPageDisplayed()
    {
        Assert.assertTrue(Productresults.size() > 0);
    }

    public PersonalisationPage clickResult()
    {
        Productresults.get(0).click();
        return new PersonalisationPage(driver);
    }


}
