Feature: User Journey to Checkout functionality

  Scenario Outline: user checking out product via kit tab menu
    Given I navigate to shop.lcfc.com page
    When I click CONTINUE button to accept Cookies
    And I click on Sign In or Register link
    And Sign In page is displayed
    And I enter the valid Sign In Details Email as "<Email>"
    And I enter the valid Sign In Details Password as "<Password>"
    And I click on the SIGN IN button
    And the user is signed In
    And I click on Kit link tab
    And the kit type page is displayed
    And I click on any of the kit type as "<Kit_Type>"
    And the kit type page is displayed
    And I click on any of the kit products
    And the kit product page is displayed
    And Choose an option for size as "<Size>"
    And I click on QTY texbox
    And I enter a valid Quantity as "<QTY>"
    And I click on ADD TO CART button
    And I click Checkout button
    And the Checkout welcome page is displayed
    And I can click on CONTINUE TO DELIVERY button
    And the shipping page is displayed
    And I click on home delivery option
    And I enter a valid receiver First Name as "<R_First_Name>"
    And I enter a valid receiver Last Name as "<R_Last_Name>"
    And I enter a valid Company as "<Company>"
    And I enter a valid Street Address Line 1 as "<Street_Address_Line_1>"
    And I enter a valid Street Address Line 2 as "<Street_Address_Line_2>"
    And I enter a valid Street Address Line 3 as "<Street_Address_Line_3>"
    And I select Country as "<Country>"
    And I enter a valid State or Province as "<State_Province>"
    And I enter a valid City as "<City>"
    And I enter a valid Zip or Postal Code as "<Zip_Postal_Code>"
    And I enter a valid Phone Number as "<Phone_Number>"
    And I select Add Delivery Methods "<Delivery_Methods>"
    And I click next button
    Then the Payment page is displayed
    And I can click on CONTINUE button to make payment


    Examples:
      | Email              | Password     | Kit_Type              | Size | QTY | R_First_Name | R_Last_Name | Company | Street_Address_Line_1 | Street_Address_Line_2 | Street_Address_Line_3 | Country        | City   | State_Province | Zip_Postal_Code | Phone_Number   | Delivery_Methods   |
      | olu.engr@gmail.com | Password@123 | SHOP 2021/22 HOME KIT | XL   | 3   | ABC          | XYZ         | Abc coy | no1                   | Church Street         | Broad way             | United Kingdom | London | Province1      | LE2 7FL         | +4474525252522 | ROYAL MAIL TRACKED |